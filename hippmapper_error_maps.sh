###### START ###############

# Denote path to manual labels and auto labels, and an output path
pathManual=$1
pathAuto=$2
subj=$3
pathT1=$pathAuto
##### Calculate_FN_FP ########
# Input variables
hemisphere=(L R)
error=(FN FP)


for dir in $pathAuto/$subj; do

    
    for h in "${hemisphere[@]}"; do

        inputManual="${pathManual}/${dir##*/}/${dir##*/}_${h}hpc_mask.nii.gz"
        inputAuto="${pathAuto}/${dir##*/}/T1brain_${h}_hippocampus_ToBrain_masked.nii.gz" #change file naming convention per method
        
        
        # Check if automatic segmentation file exists
        if [ -f "$inputAuto" ]; then
            # Calculate false negatives and false positives
            fslmaths "$inputManual" -sub "$inputAuto" -bin "${pathAuto}/${dir##*/}/${dir##*/}_${h}_FN.nii.gz"
            fslmaths "$inputAuto" -sub "$inputManual" -bin "${pathAuto}/${dir##*/}/${dir##*/}_${h}_FP.nii.gz"
        else
            echo "Error: $inputAuto doesn't exist, skipping it"
        fi
    done
done

###### BET ###########
# Brain extract the original T1 for use in FLIRT
#for dir in "$pathT1"/$subj; do
#    bet $pathT1/${dir##*/}/tool2raw.nii.gz $pathT1/${dir##*/}/${dir##*/}_brain.nii.gz -f 0.5 -g 0
#done

##### BET is not needed since ground truth images are already brain extracted and hsf_map_segs_groundtruth has aligned T1_orig images to brain extracted space 


###### FLIRT ########
# Register T1 to MNI 
for dir in "$pathT1"/$subj; do

        flirt -in "$pathManual/${dir##*/}/${dir##*/}_T1_brain.nii.gz" -ref "$FSLDIR/data/standard/MNI152_T1_2mm_brain.nii.gz" -dof 12 -out "$pathT1/${dir##*/}/${dir##*/}_toMNIlin" -omat "$pathT1/${dir##*/}/${dir##*/}_toMNIlin.mat"
        echo "Done: ${dir##*/}"

done


######## FNIRT ###########
# Transform T1 into MNI space using FNIRT
for dir in "$pathT1"/$subj; do

        fnirt --in=$pathT1/${dir##*/}/T1_orig_ToBrain.nii.gz --aff="$pathT1/${dir##*/}/${dir##*/}_toMNIlin.mat" --config=$FSLDIR/etc/flirtsch/T1_2_MNI152_2mm.cnf --iout="$pathT1/${dir##*/}/${dir##*/}_toMNInonlin" --cout="$pathT1/${dir##*/}/${dir##*/}_toMNI_coef" --fout="$pathT1/${dir##*/}/${dir##*/}_toMNI_warp"
        echo "Done: ${dir##*/}"

done

######### Warp_FN_FP_to_MNI #############
# Paths

# Variables

# Use warps from FNIRT to transform FN and FP maps into MNI space
for dir in "$pathT1"/$subj; do

    for h in "${hemisphere[@]}"; do

        for err in "${error[@]}"; do

            applywarp -i "${pathAuto}/${dir##*/}/${dir##*/}_${h}_${err}.nii.gz" -r "$FSLDIR/data/standard/MNI152_T1_1mm" -w "$pathT1/${dir##*/}/${dir##*/}_toMNI_warp.nii.gz" -o "$pathT1/${dir##*/}/${subj}_${h}_${err}_MNI.nii.gz"
        
        done
    done
    echo "Done: ${dir##*/}"
done
