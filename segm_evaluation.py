import seg_metrics.seg_metrics as sg
import pandas as pd
import os

gdth_dir    = '/vols/Data/psychiatry/BHC/SUBPROJECTS/BHC_hippo_seg'
# tool_dir  = '/vols/Data/psychiatry/BHC/SUBPROJECTS/BHC_hippo_seg/e2dhipseg_segm/run_again'
# csv_file  = os.path.join(gdth_dir,'segm_evaluation_e2dhipseg.csv')
# tool_dir  = '/vols/Data/psychiatry/BHC/SUBPROJECTS/BHC_hippo_seg/fastsurfer_segm/post_processing'
# csv_file  = os.path.join(gdth_dir,'segm_evaluation_fastsurfer.csv')
# tool_dir  = '/vols/Data/psychiatry/BHC/SUBPROJECTS/BHC_hippo_seg/first_csf-corrected_hippocampus_outputs/post_processing'
# csv_file  = os.path.join(gdth_dir,'segm_evaluation_fsl-first-csf-corrected.csv')
# tool_dir  = '/vols/Data/psychiatry/BHC/SUBPROJECTS/BHC_hippo_seg/first_hippocampus_outputs/post_processing'
# csv_file  = os.path.join(gdth_dir,'segm_evaluation_fsl-first.csv')
# tool_dir  = '/vols/Data/psychiatry/BHC/SUBPROJECTS/BHC_hippo_seg/freesurfer_hippocampus_outputs/post_processing'
# csv_file  = os.path.join(gdth_dir,'segm_evaluation_freesurfer.csv')
# tool_dir  = '/vols/Data/psychiatry/BHC/SUBPROJECTS/BHC_hippo_seg/hippmapper_jobs/run_again_fullHead/hippmapper_outputs'
# csv_file  = os.path.join(gdth_dir,'segm_evaluation_hippmapper.csv')
# tool_dir  = '/vols/Data/psychiatry/BHC/SUBPROJECTS/BHC_hippo_seg/hippodeep_segm/full_head/post_processing'
# csv_file  = os.path.join(gdth_dir,'segm_evaluation_hippodeep.csv')
# tool_dir  = '/vols/Data/psychiatry/BHC/SUBPROJECTS/BHC_hippo_seg/hippunfold_segm/subj_specific_bids/post_processing'
# csv_file  = os.path.join(gdth_dir,'segm_evaluation_hippunfold.csv')
#tool_dir    = '/vols/Data/psychiatry/BHC/SUBPROJECTS/BHC_hippo_seg/hsf_segm/post_processing'
#csv_file    = os.path.join(gdth_dir,'segm_evaluation_hsf.csv')
#tool_dir    = '/vols/Data/psychiatry/BHC/SUBPROJECTS/BHC_hippo_seg/freesurfer_hipposubfield/post_processing'
#csv_file    = os.path.join(gdth_dir,'segm_evaluation_freesurfer_hipposubfields.csv')
#tool_dir    = '/vols/Data/psychiatry/BHC/SUBPROJECTS/BHC_hippo_seg/FreeSurfer_Variants_new'
#csv_file    = os.path.join(tool_dir,'segm_evaluation_new-freesurfer.csv') # delibrately saving this to tool directory

#tool_dir    = '/vols/Data/psychiatry/BHC/SUBPROJECTS/BHC_hippo_seg/FreeSurfer_Variants_new/FreeSurfer_HippoSubfield_segs'
#csv_file    = os.path.join(tool_dir,'segm_evaluation_new-freesurfer-hipposubfields.csv') # delibrately saving this to tool directory

tool_dir    = '/vols/Data/psychiatry/BHC/SUBPROJECTS/BHC_hippo_seg/FreeSurfer_Variants_new/FastSurfer_hipp_segs'
csv_file    = os.path.join(tool_dir,'segm_evaluation_new-fastsurfer.csv') # delibrately saving this to tool directory


# output from subj HP10 and HP16 not present for freesurfer & hipposubfield
#subjects=["HP01", "HP02", "HP03", "HP04", "HP05", "HP06", "HP07", "HP08", "HP09", "HP11", "HP12", "HP14", "HP15", "HP17", "HP18", "HP19", "HP20", "HP21", "HP22", "HP23", "HP24", "HP25", "HP26","HP27", "HP28", "HP29", "HP30"]

# subjects=["HP01", "HP02", "HP03", "HP04", "HP05", "HP06", "HP07", "HP08", "HP09", "HP10", "HP11", "HP12", "HP14", "HP15", "HP16", "HP17", "HP18", "HP19", "HP20", "HP21", "HP22", "HP23", "HP24", "HP25", "HP26","HP27", "HP28", "HP29", "HP30"]

# for new freesurfer-based mapping HP10 has freesurfer error; HP 13 doesnt have ground truth
#subjects=["HP01", "HP02", "HP03", "HP04", "HP05", "HP06", "HP07", "HP08", "HP09", "HP11", "HP12", "HP14", "HP15", "HP16", "HP17", "HP18", "HP19", "HP20", "HP21", "HP22", "HP23", "HP24", "HP25", "HP26","HP27", "HP28", "HP29", "HP30"]

# for new freesurfer-subfields HP 10 has freesurfer error; HP13 doesnt have ground truth; HP16 doesnt have aseg.mgz - removing for consistency with previous ones
#subjects=["HP01", "HP02", "HP03", "HP04", "HP05", "HP06", "HP07", "HP08", "HP09", "HP11", "HP12", "HP14", "HP15", "HP17", "HP18", "HP19", "HP20", "HP21", "HP22", "HP23", "HP24", "HP25", "HP26","HP27", "HP28", "HP29", "HP30"]

# for new fastsurfer only HP13 not taken since no ground truth available
subjects=["HP01", "HP02", "HP03", "HP04", "HP05", "HP06", "HP07", "HP08", "HP09", "HP10", "HP11", "HP12", "HP14", "HP15", "HP16", "HP17", "HP18", "HP19", "HP20", "HP21", "HP22", "HP23", "HP24", "HP25", "HP26","HP27", "HP28", "HP29", "HP30"]

hemisphere=['L', 'R']

segmentation_metrics = []

for subj in subjects:
    
    for h in hemisphere:
        
        # only binary masks
        labels = [1]
        
        # ground truth file
        gdth_file = os.path.join(gdth_dir,subj,subj + '_' + h + 'hpc_mask.nii.gz') 
        
        # Hippocampus image names from each tool (aligned to the ground truth): 
        # 1) e2dhipseg:                 T1_orig_L_hippocampus_ToBrain_binarised
        # 2) fastsurfer:                T1_orig_L_hippocampus_ToBrain_binarised
        # 3) first_csf-corrected:       T1_first_all_fast_firstseg_L_hipp_masked_0_7
        # 4) first:                     T1_first_all_fast_firstseg_${h}_hipp
        # 5) freesurfer:                T1_orig_L_hippocampus_ToBrain_binarised
        # 6) hippmapper:                T1brain_L_hippocampus_ToBrain_masked
        # 7) hippodeep:                 T1_orig_L_hippocampus_ToBrain_binarised
        # 8) hippunfold:                $subj_L_hippocampus_ToBrain_binarised
        # 9) hsf:                       T1_orig_L_hippocampus_ToBrain_binarised
        # 10) freesurfer hipposubfield: T1_orig_L_hippocampus_ToBrain_binarised
        # 11) new freesurfer:           $subj_L_hippocampus_mask.nii.gz            
        # 12) new freesurfer hipposubfield: $subj_L_hippocampus_mask.nii.gz 
        # 13) new fastsurfer: $subj_L_hippocampus_mask.nii.gz

        #pred_file = os.path.join(tool_dir, subj, 'T1_orig_' + h + '_hippocampus_ToBrain_binarised.nii.gz')

        # For new freesurfer, also subfields, also fastsurfer - since we have subjid in filenames
        pred_file = os.path.join(tool_dir, subj, subj + '_' + h + '_hippocampus_mask.nii.gz')

        print('%s subject done for %s hemisphere'%(subj,h))
        
        metrics = sg.write_metrics(labels, gdth_file, pred_file, metrics=['dice', 'hd', 'hd95','vs'])
        
        #Store metrics along with subject and hemisphere
        segmentation_metrics.append({
            'Subject': subj,
            'Hemisphere': h,
            'Metrics': metrics})

#Convoluted routine to get the values out of the segmentation metrics list/dictionary/list/dictionary 
all_metrics = []

# Loop through the segmentation_metrics list
for entry in segmentation_metrics:
    subject = entry['Subject']
    hemisphere = entry['Hemisphere']
    metrics_list = entry['Metrics']  
    
    # Loop through the metrics_list, which is a list containing a dictionary
    for metrics in metrics_list:
        dice_coefficient = metrics['dice']
        hausdorff_distance = metrics['hd']
        hd95 = metrics['hd95']
        volume_similarity = metrics['vs']
        
        # Create a list to represent the data for each metric entry
        metric_data = [subject, hemisphere, dice_coefficient, hausdorff_distance, hd95, volume_similarity]
        
        # Append the metric data to the all_metrics list
        all_metrics.append(metric_data)
        
# Create a DataFrame from the list of metrics
df = pd.DataFrame(all_metrics, columns=['Subject', 'Hemisphere', 'Dice Coefficient', 'Hausdorff Distance', 'HD95', 'Volume Similarity'])
        
# Save the DataFrame to a CSV file
df.to_csv(csv_file, index=False)
