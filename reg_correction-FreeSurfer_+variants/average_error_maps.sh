proc_dir="/vols/Data/psychiatry/BHC/SUBPROJECTS/BHC_hippo_seg/FreeSurfer_Variants_new/FreeSurfer_hippSubfields_segs"

# create error_maps directory in post_processing
mkdir -p $proc_dir/error_maps

# copy error maps to error_maps directory
for dir in HP*;do(cd "$dir" && cp *_MNI.nii.gz $proc_dir/error_maps);done

cd $proc_dir/error_maps

# make .txt lists
ls *_L_FN_MNI.nii.gz > L_FN_filenames_warped.txt
ls *_L_FP_MNI.nii.gz > L_FP_filenames_warped.txt
ls *_R_FN_MNI.nii.gz > R_FN_filenames_warped.txt
ls *_R_FP_MNI.nii.gz > R_FP_filenames_warped.txt

# Variables
hemisphere=(L R)
error=(FN FP)

for h in "${hemisphere[@]}"; do
    for err in "${error[@]}"; do
        fsladd "${err}_${h}_avg_warped" -m  $(ls *_${h}_${err}_MNI.nii.gz)
    done
done
