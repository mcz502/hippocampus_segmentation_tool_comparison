indir='/vols/Data/psychiatry/BHC/SUBPROJECTS/BHC_hippo_seg/FreeSurfer_Variants_new/FreeSurfer_hipp_segs'

for subj_dir in $indir/HP*; do

    cd $subj_dir

    subj=$(basename "$subj_dir")

    mri_label2vol --seg aseg.auto_noCCseg.mgz --temp rawavg.mgz --o ${subj_dir}/${subj}_resampled_aseg.mgz --regheader aseg.auto_noCCseg.mgz

    mri_binarize --i ${subj_dir}/${subj}_resampled_aseg.mgz --match 17 --o ${subj_dir}/${subj}_L_hippocampus_mask.mgz
    mri_binarize --i ${subj_dir}/${subj}_resampled_aseg.mgz --match 53 --o ${subj_dir}/${subj}_R_hippocampus_mask.mgz

    mri_convert ${subj_dir}/${subj}_L_hippocampus_mask.mgz ${subj_dir}/${subj}_L_hippocampus_mask.nii.gz
    mri_convert ${subj_dir}/${subj}_R_hippocampus_mask.mgz ${subj_dir}/${subj}_R_hippocampus_mask.nii.gz

    echo "Completed: $subj"
done