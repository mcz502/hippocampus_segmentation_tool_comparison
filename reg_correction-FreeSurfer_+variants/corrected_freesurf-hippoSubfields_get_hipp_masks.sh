indir='/vols/Data/psychiatry/BHC/SUBPROJECTS/BHC_hippo_seg/FreeSurfer_Variants_new/FreeSurfer_hippSubfields_segs'

for subj_dir in $indir/HP*; do

    cd $subj_dir

    subj=$(basename "$subj_dir")

    mri_label2vol --seg lh.hippoAmygLabels-T1-AN.v21.FSvoxelSpace.mgz --temp rawavg.mgz --o ${subj_dir}/${subj}_lh_resampled_aseg.mgz --regheader lh.hippoAmygLabels-T1-AN.v21.FSvoxelSpace.mgz
    mri_label2vol --seg rh.hippoAmygLabels-T1-AN.v21.FSvoxelSpace.mgz --temp rawavg.mgz --o ${subj_dir}/${subj}_rh_resampled_aseg.mgz --regheader rh.hippoAmygLabels-T1-AN.v21.FSvoxelSpace.mgz


    mri_binarize --i ${subj_dir}/${subj}_lh_resampled_aseg.mgz --match 203 211 212 215 226 233 234 235 236 237 238 239 240 241 242 243 244 245 246 --o ${subj_dir}/${subj}_L_hippocampus_mask.mgz
    mri_binarize --i ${subj_dir}/${subj}_rh_resampled_aseg.mgz --match 203 211 212 215 226 233 234 235 236 237 238 239 240 241 242 243 244 245 246 --o ${subj_dir}/${subj}_R_hippocampus_mask.mgz

    mri_convert ${subj_dir}/${subj}_L_hippocampus_mask.mgz ${subj_dir}/${subj}_L_hippocampus_mask.nii.gz
    mri_convert ${subj_dir}/${subj}_R_hippocampus_mask.mgz ${subj_dir}/${subj}_R_hippocampus_mask.nii.gz

    echo "Completed: $subj"
done