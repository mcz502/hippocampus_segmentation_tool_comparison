seg_dir="/vols/Data/psychiatry/BHC/SUBPROJECTS/BHC_hippo_seg/freesurfer_hipposubfield/post_processing"

for dir in $seg_dir/HP*; do
    cd $dir 
    echo "Checking ${dir##*/}..."

    if ! [ -f "rh_hipp.nii.gz" ]; then
        input_left_img="lh.hippoAmygLabels-T1-AN.v21.FSvoxelSpace.mgz"
        input_right_img="rh.hippoAmygLabels-T1-AN.v21.FSvoxelSpace.mgz"

        mri_binarize --i $input_left_img --match 203 211 212 215 226 233 234 235 236 237 238 239 240 241 242 243 244 245 246 --o lh_hipp.mgz
        mri_binarize --i $input_right_img --match 203 211 212 215 226 233 234 235 236 237 238 239 240 241 242 243 244 245 246 --o rh_hipp.mgz

        mri_convert lh_hipp.mgz lh_hipp.nii.gz
        mri_convert rh_hipp.mgz rh_hipp.nii.gz

        echo "${dir##*/}: Finished"
    else
        echo "${dir##*/}: outputs already exist..proceeding to next subject"
    fi
done

