seg_dir="/vols/Data/psychiatry/BHC/SUBPROJECTS/BHC_hippo_seg/hippmapper_jobs/run_again_fullHead/hippmapper_outputs"
ref_dir="/vols/Data/psychiatry/BHC/SUBPROJECTS/BHC_hippo_seg"

for dir in $seg_dir/HP*; do
    cd $dir
    echo "Checking ${dir##*/}..."
    if [ -f ${dir##*/}_T1_orig_std_orient.nii.gz ]; then
       echo "${dir##*/}_T1_brain_std_orient.nii.gz exists...processing further..."
              
       if ! [ -f "T1brain_R_hippocampus_ToBrain_masked.nii.gz" ]; then 
          # Divide the segmentation output to left and right hemispheres 
          fslcpgeom ${dir##*/}_T1_orig.nii.gz ${dir##*/}_hippmapper.nii.gz
          fslmaths ${dir##*/}_hippmapper.nii.gz -thr 2 -bin hipp_L
          fslmaths ${dir##*/}_hippmapper.nii.gz -uthr 1 -bin hipp_R
          
          input_left_img="hipp_L.nii.gz"
          input_right_img="hipp_R.nii.gz"
          ref_img=$ref_dir/${dir##*/}/${dir##*/}_T1_brain.nii.gz
          
          # Apply FLIRT transformations to map them on original brain image
          flirt -in $input_left_img -ref $ref_img -applyxfm -usesqform -out T1brain_L_hippocampus_ToBrain.nii.gz
          flirt -in $input_right_img -ref $ref_img -applyxfm -usesqform -out T1brain_R_hippocampus_ToBrain.nii.gz
       
          # Binarize them to create masks for further comparison
          fslmaths T1brain_L_hippocampus_ToBrain.nii.gz -thr 0.5 -bin T1brain_L_hippocampus_ToBrain_masked.nii.gz
          fslmaths T1brain_R_hippocampus_ToBrain.nii.gz -thr 0.5 -bin T1brain_R_hippocampus_ToBrain_masked.nii.gz
          echo "${dir##*/}: Finished"
       else
          echo "${dir##*/}: outputs already exist..proceeding to next subject" 
       fi
        
    else
        cd $dir
        echo "${dir##*/}_T1_brain_std_orient.nii.gz does not exist...skip FLIRT processing..."
        
        if ! [ -f  "T1brain_R_hippocampus_ToBrain_masked.nii.gz" ]; then
           # Divide the segmentation output to left and right hemispheres 
           fslmaths ${dir##*/}_hippmapper.nii.gz -thr 2 -bin hipp_L
           fslmaths ${dir##*/}_hippmapper.nii.gz -uthr 1 -bin hipp_R

           input_left_img="hipp_L.nii.gz"
           input_right_img="hipp_R.nii.gz"
           ref_img=$ref_dir/${dir##*/}/${dir##*/}_T1_brain.nii.gz
  
           flirt -in $input_left_img -ref $ref_img -applyxfm -usesqform -out T1brain_L_hippocampus_ToBrain.nii.gz
           flirt -in $input_right_img -ref $ref_img -applyxfm -usesqform -out T1brain_R_hippocampus_ToBrain.nii.gz

           fslmaths T1brain_L_hippocampus_ToBrain.nii.gz -thr 0.5 -bin T1brain_L_hippocampus_ToBrain_masked.nii.gz
           fslmaths T1brain_R_hippocampus_ToBrain.nii.gz -thr 0.5 -bin T1brain_R_hippocampus_ToBrain_masked.nii.gz
        
           echo "${dir##*/}: Finished"
        else
          echo "${dir##*/}: outputs already exist..proceeding to next subject"
        fi
    fi
done
# Transform T1_orig to T1_brain space
for dir in $seg_dir/HP*; do
    cd $dir
    echo "Checking ${dir##*/}..."

       if ! [ -f T1_orig_ToBrain.nii.gz ]; then


          input_img=${dir##*/}_T1_orig.nii.gz
          ref_img=$ref_dir/${dir##*/}/${dir##*/}_T1_brain.nii.gz

          # Apply FLIRT with rigid body transformations to map them on ground truth image
          flirt -in $input_img -ref $ref_img -applyxfm -usesqform -out T1_orig_ToBrain.nii.gz
          echo "${dir##*/}: Finished"
       else
          echo "${dir##*/}: outputs already exist..proceeding to next subject"
       fi
done