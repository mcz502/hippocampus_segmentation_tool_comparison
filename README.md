### Compare hippocampus segmentation from various tools with ground truth
---

Tools used:

1. e2dhip Segmentation
2. hippodeep Segmentation
3. hippunfold Segmentation 
4. hsf Segmentation
5. Fastsurfer Segmentation 
6. FSL-FIRST CSF corrected Segmentation 
7. FSL-FIRST Segmentation 
8. FreeSurfer Segmentation 
9. hippmapper Segmentation 
10. FreeSurfer hippocampus subfield segmentation
---

##### Steps: 

1) Run the tool
2) Map hippocampus segmentation masks on the ground truth images
3) Create the error maps
4) Visualise the error maps
5) Get segmentation evaluation metrics

---

##### Updated

For three tools [FreeSurfer, FreeSurfer-hippocampus-subfield-segmentation, FastSurfer] the mapping of the hippocampus masks to original images is corrected. Codes are provided in `reg_correction-FreeSurfer_+variants` 
