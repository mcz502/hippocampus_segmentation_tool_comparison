seg_dir="/vols/Data/psychiatry/BHC/SUBPROJECTS/BHC_hippo_seg/hippunfold_segm/subj_specific_bids/post_processing"
ref_dir="/vols/Data/psychiatry/BHC/SUBPROJECTS/BHC_hippo_seg"
# Transform masks
for dir in $seg_dir/HP*; do
    cd $dir
    echo "Checking ${dir##*/}..."

       if ! [ -f ${dir##*/}_R_hippocampus_ToBrain_binarised.nii.gz ]; then


          input_L_img=${dir##*/}_hipp_L_masked.nii.gz
          input_R_img=${dir##*/}_hipp_R_masked.nii.gz
          ref_img=$ref_dir/${dir##*/}/${dir##*/}_T1_brain.nii.gz

          # Apply FLIRT with rigid body transformations to map them on ground truth image
          flirt -in $input_L_img -ref $ref_img -applyxfm -usesqform -out ${dir##*/}_L_hippocampus_ToBrain.nii.gz
          flirt -in $input_R_img -ref $ref_img -applyxfm -usesqform -out ${dir##*/}_R_hippocampus_ToBrain.nii.gz

          # Binarize them to create masks for further comparison
          fslmaths ${dir##*/}_L_hippocampus_ToBrain.nii.gz -thr 0.5 -bin ${dir##*/}_L_hippocampus_ToBrain_binarised.nii.gz
          fslmaths ${dir##*/}_R_hippocampus_ToBrain.nii.gz -thr 0.5 -bin ${dir##*/}_R_hippocampus_ToBrain_binarised.nii.gz

          echo "${dir##*/}: Finished"
       else
          echo "${dir##*/}: outputs already exist..proceeding to next step"
       fi
done
# Transform ${dir##*/} to T1_brain space
for dir in $seg_dir/HP*; do
    cd $dir
    echo "Checking ${dir##*/}..."

       if ! [ -f T1_orig_ToBrain.nii.gz ]; then


          input_img="T1_orig.nii.gz"
          ref_img=$ref_dir/${dir##*/}/${dir##*/}_T1_brain.nii.gz

          # Apply FLIRT with rigid body transformations to map them on ground truth image
          flirt -in $input_img -ref $ref_img -applyxfm -usesqform -out ${dir##*/}_ToBrain.nii.gz
          echo "${dir##*/}: Finished"
       else
          echo "${dir##*/}: outputs already exist..proceeding to next subject"
       fi
done