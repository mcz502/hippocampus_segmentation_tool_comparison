seg_dir="/vols/Data/psychiatry/BHC/SUBPROJECTS/BHC_hippo_seg/freesurfer_hipposubfield/post_processing"
ref_dir="/vols/Data/psychiatry/BHC/SUBPROJECTS/BHC_hippo_seg"
# Transform masks
for dir in $seg_dir/HP*; do
    cd $dir
    echo "Checking ${dir##*/}..."

       if ! [ -f "${dir##*/}_R_hippocampus_ToBrain_binarised.nii.gz" ]; then


          input_left_img="lh_hipp.nii.gz"
          input_right_img="rh_hipp.nii.gz"
          ref_img=$ref_dir/${dir##*/}/${dir##*/}_T1_brain.nii.gz

          # Apply FLIRT with rigid body transformations to map them on ground truth image
          flirt -in $input_left_img -ref $ref_img -applyxfm -usesqform -out T1_orig_L_hippocampus_ToBrain.nii.gz
          flirt -in $input_right_img -ref $ref_img -applyxfm -usesqform -out T1_orig_R_hippocampus_ToBrain.nii.gz

          # Binarize them to create masks for further comparison
          fslmaths T1_orig_L_hippocampus_ToBrain.nii.gz -thr 0.5 -bin T1_orig_L_hippocampus_ToBrain_binarised.nii.gz
          fslmaths T1_orig_R_hippocampus_ToBrain.nii.gz -thr 0.5 -bin T1_orig_R_hippocampus_ToBrain_binarised.nii.gz

          echo "${dir##*/}: Finished"
       else
          echo "${dir##*/}: outputs already exist..proceeding to next step"
       fi
done
